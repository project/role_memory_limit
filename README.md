## Role Memory Limit

Role memory limit is a small module that allows you to
set php_memory_limit per role and for command line usage
(drush).

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/1810870

### Usage example
Let's say your php_memory_limit is set to 128mb
and your site has a lot of blocks, and you try to get into
block listing page and drupal crashes.

That is because there isn't enough memory.

With this module, you can set it up, so the admin has more
php_memory_limit. That way you can render pages that require
a lot of memory but still keep the original php_memory_limit
for other users.

### Requirements

Permission to change php_memory_limit on your web server.

No other special requirements needed.

### Install

Please install via composer method.

```bash
composer require drupal/role_memory_limit
drush en role_memory_limit
```

### Configuration

#### Drupal 8+
Goto _/admin/config/system/role-memory-limit_ to configure memory limits per role.

#### Drupal 7
Goto _admin/people/permissions#module-role_memory_limit_ to set the permissions.

Goto _admin/config/development/role_memory_limit_ to set the limit for a role.

### Maintainers

Current maintainers:
* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
