<?php

/**
 * @file
 * Drush command altering for role_memory_limit.
 */

/**
 * Implements hook_drush_command_alter().
 */
function role_memory_limit_drush_command_alter(&$command) {
  // Sets the PHP memory limit for drush.
  $config = Drupal::config('role_memory_limit.config');
  if (empty($config->get('drush'))) {
    return;
  }
  ini_set('memory_limit', $config->get('drush') . 'M');
}
