<?php

namespace Drupal\role_memory_limit\EventSubscriber;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Role memory limit event subscriber.
 */
class RoleMemoryLimit implements EventSubscriberInterface {

  /**
   * The user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $user;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new RoleMemoryLimit object.
   *
   * @param \Drupal\Core\Session\AccountProxy $user
   *   The user.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config.
   */
  public function __construct(AccountProxy $user, ConfigFactory $config) {
    $this->user = $user;
    $this->config = $config->get('role_memory_limit.config')->getRawData();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST] = ['setMemoryLimit'];
    return $events;
  }

  /**
   * Sets the PHP memory limit based on the users' role.
   */
  public function setMemoryLimit() {
    if (empty($this->config)) {
      return;
    }

    $memory_limits = [];
    if ($this->user->id() == '1') {
      $memory_limits[] = $this->config['administrator'];
    }
    else {
      foreach ($this->user->getRoles() as $role) {
        $memory_limits[] = $this->config[$role];
      }
    }

    // Take the highest memory limit.
    $mb = min($memory_limits) < 0 ? -1 : max($memory_limits);
    if ($mb) {
      if ($mb != '-1') {
        $mb .= 'M';
      }
      ini_set('memory_limit', $mb);
    }
  }

}
