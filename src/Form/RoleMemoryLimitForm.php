<?php

namespace Drupal\role_memory_limit\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Role memory limit config form.
 */
class RoleMemoryLimitForm extends ConfigFormBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'role_memory_limit',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'role_memory_limit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('role_memory_limit.config');

    $roles = $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();

    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'] = ['role_memory_limit/admin'];

    $form['current_memory_limit'] = [
      '#markup' => '<p>' . $this->t('Current memory limit <em>@memory_limit</em>.', ['@memory_limit' => ini_get('memory_limit')]) . '</p>',
    ];

    $form['instructions'] = [
      '#markup' => '<p>' . $this->t('Enter the memory in megabytes or -1 for unlimited.<br />Leave empty for default.') . '</p>',
    ];

    $form['roles'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Roles'),
    ];
    foreach ($roles as $role) {
      $form['roles'][$role->id()] = [
        '#type' => 'textfield',
        '#required' => FALSE,
        '#title' => $role->label(),
        '#size' => 10,
        '#default_value' => $config->get($role->id()),
        '#suffix' => 'MB',
        '#input_group' => TRUE,
      ];
      $form['#form_keys'][] = $role->id();
    }

    $form['other'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Other'),
    ];
    $form['other']['drush'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Drush'),
      '#size' => 10,
      '#default_value' => $config->get('drush'),
      '#suffix' => 'MB',
    ];
    $form['#form_keys'][] = 'drush';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($form['#form_keys'] as $item) {
      if ($values[$item] === '' || $values[$item] === '-1') {
        continue;
      }
      if (!is_numeric($values[$item])) {
        $form_state->setErrorByName($item, $this->t('Numbers only please.'));
        continue;
      }
      if ($values[$item] < 64) {
        $form_state->setErrorByName($item, $this->t('Minimum allowed is 64 MB.'));
      }
      if ($values[$item] > 2048) {
        $form_state->setErrorByName($item, $this->t('Maximum allowed is 2048 MB.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('role_memory_limit.config');

    foreach ($form['#form_keys'] as $item) {
      $config->set($item, $values[$item]);
    }

    $config->save();
  }

}
